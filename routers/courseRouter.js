// Khoi toa 1 thu vien express
const express = require ('express');

const course = express.Router();
//import course Controller
const {createCourse} = require('../controllers/courseController')


course.get("/course",createCourse)

// Export dữ liệu tành 1 module
module.exports = course 