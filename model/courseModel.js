// B1 imprort mongooseJS
const mongoose = require('mongoose')

// B2 Khai bao Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3 khởi tạo 1 Schema với các thuộc tính được yêu cầu
const course = new Schema({
    _id: { type: mongoose.Types.ObjectId },
    courseCode: { type: String, require: true, unique: true },
    courseName: { type: String, require: true },
    price: { type: Number, require: true },
    discountPrice: { type: Number, require: true },
    duration: { type: String, require: true },
    level: { type: String, require: true },
    coverImage: { type: String, require: true },
    teacherName: { type: String, require: true },
    teacherPhoto: { type: String, require: true },
    isPopular: { type: Boolean, default: true },
    isTrending: { type: Boolean, default: false },
    ngayTao: { type: Date, default: Date.now() },
    ngayCapNhat: { type: Date, default: Date.now() }
})
// B4: Export ra một model Schema vừa khai báo
module.exports = mongoose.model("course", course);