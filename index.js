// import express
const express = require('express');

// Import Mongoose
const mongoose = require('mongoose');

// Khoi tao express
const app = express();

// Import router
const courseRouter = require('./routers/courseRouter')
// khai bao cong project
const port = 8000;

// Cấu hình để app đọc được body request dạng json
app.use(express.json());

// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

// Khai bao mongoose 
mongoose.connect("mongodb://localhost:27017/Course", (err) => {
    if (err) {
        throw err;
    }
    console.log("Connect MongoDB Successfully!");
})
// chay app co hinh anh
app.use(express.static(__dirname + `/views`))

// Khai bao API dang GET "/" sẽ chạy vào đây
// Callback function: là 1 tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    response.sendFile(path.john(__dirname + '/views/index.html'));
})

app.use("/",courseRouter);
// chay app express
app.listen(port, () => {
    console.log("Ung Dung Dang Chay tren cong: " + port)
})