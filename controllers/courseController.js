const courseModel = require("../model/courseModel")
// Khai bao mongoose
const mongoose = require('mongoose');


// tao course
const createCourse = (request, response) => {
    //B1 Thu thap du lieu
    let bodyRequest = request.body
    console.log(bodyRequest)
    //B2 validate du lieu
    if (!bodyRequest.courseCode) {
        response.status(400).json({
            status: "Error",
            message: "Vui Long Nhap courseCode"
        })
    }

    if (!(Number.isInteger(bodyRequest.price) && bodyRequest.price > 0 )) {
        response.status(400).json({
            status: "Loi roi",
            message: "Vui Long Nhap Gia"
        })
    }
    if (!(Number.isInteger(bodyRequest.discountPrice) && bodyRequest.discountPrice > 0 )) {
        response.status(400).json({
            status: "Loi roi",
            message: "Vui Long Nhap Ma Giam"
        })
    }

    // B3 Thao tac voi du lieu
    let createCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: bodyRequest.courseCode,
        courseName: bodyRequest.courseName,
        price: bodyRequest.price,
        discountPrice: bodyRequest.discountPrice,
        duration: bodyRequest.duration,
        level: bodyRequest.level,
        coverImage: bodyRequest.coverImage,
        teacherName: bodyRequest.teacherName,
        teacherPhoto: bodyRequest.teacherPhoto,
        isPopular: bodyRequest.isPopular,
        isTrending: bodyRequest.isTrending,
    }
    courseModel.create(createCourse, (eror, data) => {
        if (eror) {
            response.status(500).json({
                status: "Err 500: Internal server error",
                message: eror.message
            })
        } else {
            response.status(201).json({
                status: "Success: Course created",
                message: data
            })
        }
    })
};

// Export controller than 1 module la 1 object gom cac ham trong controller
module.exports = {
    createCourse: createCourse,

}