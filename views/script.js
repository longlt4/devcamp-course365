/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
var gCoursesPopular = [];
var gCoursesTrending = [];

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    // hàm lấy obj courses Popular
    coursesPopular();
    // hàm lấy obj courses Trending
    coursesTrending();
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */



/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// hàm xử lý lọc lấy obj courses Popular
function coursesPopular() {
    for (var i = 0; i < gCoursesDB.courses.length; i++) {
        if (gCoursesDB.courses[i].isPopular === true) {
            gCoursesPopular = (gCoursesDB.courses[i])
            let Card = `
            <div class="col-sm-3 border-light mb-3">
            <div class="card">
                <div class="card-header">
                    <img class="card-img-top rounded-top" src="${gCoursesPopular.coverImage}" alt="">
                </div>
                <div class="card-body">
                    <h6 class="card-title text-primary">${gCoursesPopular.courseName}</h6>
                    <p class="card-text"><i class="fa-regular fa-clock"></i> ${gCoursesPopular.duration} &nbsp; ${gCoursesPopular.level}</p>
        
                    <p class="card-text">
                        <span class="font-weight-bold">${gCoursesPopular.price}</span> <span class="text-secondary"><del>${gCoursesPopular.discountPrice} $550</del></span>
                    </p>
                </div>
                <div class="card-footer form-inline justify-content-between">
                    <img class="rounded-circle w-25" style="max-width:20%;" src=${gCoursesPopular.teacherPhoto} alt="">
                        <p class="card-text form-inline w-75 justify-content-between"><small> ${gCoursesPopular.teacherName}</small> <i
                            class="fa-regular fa-bookmark"></i></p>
                </div>
            </div>
        </div>
             `

            document.querySelector(".Popular").innerHTML += Card;
        }
    }
}
// hàm xử lý lọc lấy obj courses Trending
function coursesTrending() {
    for (var i = 0; i < gCoursesDB.courses.length; i++) {
        if (gCoursesDB.courses[i].isTrending === true) {
            gCoursesTrending = (gCoursesDB.courses[i])
            let Card = `
            <div class="col-sm-3 border-light mb-3">
            <div class="card">
                <div class="card-header">
                    <img class="card-img-top rounded-top" src="${gCoursesTrending.coverImage}" alt="">
                </div>
                <div class="card-body">
                    <h6 class="card-title text-primary">${gCoursesTrending.courseName}</h6>
                    <p class="card-text"><i class="fa-regular fa-clock"></i> ${gCoursesTrending.duration} &nbsp; ${gCoursesTrending.level}</p>
        
                    <p class="card-text">
                        <span class="font-weight-bold">${gCoursesTrending.price}</span> <span class="text-secondary"><del>${gCoursesTrending.discountPrice} $550</del></span>
                    </p>
                </div>
                <div class="card-footer form-inline justify-content-between">
                    <img class="rounded-circle w-25" style="max-width:20%;" src=${gCoursesTrending.teacherPhoto} alt="">
                        <p class="card-text form-inline w-75 justify-content-between"><small> ${gCoursesTrending.teacherName}</small> <i
                            class="fa-regular fa-bookmark"></i></p>
                </div>
            </div>
        </div>
             `

            document.querySelector(".Trending").innerHTML += Card;

            console.log(Card)
        }
    }
}


